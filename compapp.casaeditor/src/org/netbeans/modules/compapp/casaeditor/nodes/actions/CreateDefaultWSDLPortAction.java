package org.netbeans.modules.compapp.casaeditor.nodes.actions;

import java.awt.Point;
import java.util.*;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.netbeans.modules.compapp.casaeditor.model.casa.*;
import org.netbeans.modules.compapp.casaeditor.nodes.ServiceUnitNode;
import org.netbeans.modules.compapp.projects.jbi.api.JbiBindingInfo;
import org.netbeans.modules.compapp.projects.jbi.api.JbiDefaultComponentInfo;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

/**
 * Action to create default SOAP port for all unset endpoint.
 *
 * @author David BRASSELY
 */
public class CreateDefaultWSDLPortAction extends NodeAction {

    public CreateDefaultWSDLPortAction() {
    }

    protected void performAction(Node[] activatedNodes) {
        if (activatedNodes.length > 0 && activatedNodes[0] instanceof ServiceUnitNode) {
            ServiceUnitNode suNode = (ServiceUnitNode) activatedNodes[0];
            CasaWrapperModel casaModel = suNode.getModel();
            CasaServiceEngineServiceUnit casaSESU =
                    (CasaServiceEngineServiceUnit) suNode.getData();

            final Point location = new Point(-1, -1);
            final CasaWrapperModel model = casaModel;
            final CasaServiceEngineServiceUnit serviceUnit = casaSESU;
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    showDialog(model, serviceUnit, location);
                }
            });
        }
    }

    protected boolean enable(Node[] activatedNodes) {
        return activatedNodes.length > 0 && activatedNodes[0] instanceof ServiceUnitNode;
    }

    public Action getAction() {
        return this;
    }

    protected boolean asynchronous() {
        return false;
    }

    public String getName() {
        return NbBundle.getMessage(CreateDefaultWSDLPortAction.class,
                "LBL_CreateDefaultWSDLPortAction_Name"); // NOI18N
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    private void showDialog(final CasaWrapperModel model,
            final CasaServiceEngineServiceUnit serviceUnit, final Point location) {

        JbiBindingInfo soapBc = null;

        JbiDefaultComponentInfo bcinfo = JbiDefaultComponentInfo.getJbiDefaultComponentInfo();
        if (bcinfo != null) {
            List<JbiBindingInfo> bclist = bcinfo.getBindingInfoList();
            for (JbiBindingInfo bi : bclist) {
                if (bi.getBindingType().equals("soap")) {
                    soapBc = bi;
                    break;

                }
            }
        }

        if (soapBc == null) {
            return;
        }

        for (CasaEndpointRef ep : serviceUnit.getEndpoints()) {
            List<CasaConnection> cons = model.getConnections(ep, false);
            if (cons.isEmpty()) {
                CasaPort cs = model.addCasaPort(
                        soapBc.getBindingType(),
                        soapBc.getBindingComponentName(),
                        location.x,
                        location.y);

                model.addConnection(
                        (CasaConsumes) cs.getConsumes(), (CasaProvides) ep);
            }
        }
    }
    /*
     * final List<CasaEndpointRef> endpoints = new ArrayList<CasaEndpointRef>();
     *
     * for (CasaPort casaPort : model.getCasaPorts()) { CasaEndpointRef consumes
     * = casaPort.getConsumes(); if (model.canConnect(consumes, endpoint)) {
     * endpoints.add(consumes); } CasaEndpointRef provides =
     * casaPort.getProvides(); if (model.canConnect(provides, endpoint)) {
     * endpoints.add(provides); } }
     *
     * for (CasaServiceEngineServiceUnit sesu :
     * model.getServiceEngineServiceUnits()) { for (CasaEndpointRef ep :
     * sesu.getEndpoints()) { if (model.canConnect(ep, endpoint)) {
     * endpoints.add(ep); } } }
     *
     * Collections.sort(endpoints, new Comparator<CasaEndpointRef>() {
     *
     * public int compare(CasaEndpointRef o1, CasaEndpointRef o2) { String
     * endpointName1 = o1.getEndpointName(); String endpointName2 =
     * o2.getEndpointName(); return endpointName1.compareTo(endpointName2); }
     * });
     *
     * final EndpointSelectionPanel panel = new
     * EndpointSelectionPanel(endpoints); DialogDescriptor descriptor = new
     * DialogDescriptor( panel,
     * NbBundle.getMessage(EndpointSelectionPanel.class,
     * "LBL_Endpoint_Selection_Panel"), // NOI18N true, new ActionListener() {
     *
     * public void actionPerformed(ActionEvent evt) { if
     * (evt.getSource().equals(DialogDescriptor.OK_OPTION)) { CasaEndpointRef ep
     * = panel.getSelectedItem(); if (ep != null) { if (ep instanceof
     * CasaConsumes) { model.addConnection( (CasaConsumes) ep, (CasaProvides)
     * endpoint); } else { model.addConnection( (CasaConsumes) endpoint,
     * (CasaProvides) ep); } } } } });
     *
     * Dialog dlg = DialogDisplayer.getDefault().createDialog(descriptor);
     * dlg.setPreferredSize(new Dimension(400, 400)); dlg.setVisible(true);
     *
     */
}
